import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
let AuthGuardService = class AuthGuardService {
    constructor(router, authService) {
        this.router = router;
        this.authService = authService;
    }
    canActivate(route, state) {
        if (this.authService.getCookie('user')) {
            // logged in so return true
            this.authService.currentUser = this.authService.getCookie('user');
            return true;
        }
        return false;
    }
};
AuthGuardService = __decorate([
    Injectable({ providedIn: 'root' })
], AuthGuardService);
export { AuthGuardService };
//# sourceMappingURL=auth-guard.service.js.map