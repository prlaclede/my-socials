import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
let SocialDialogComponent = class SocialDialogComponent {
    constructor(dialogRef, http, data) {
        this.dialogRef = dialogRef;
        this.http = http;
        this.data = data;
    }
    save() {
        var url = (this.data._id) ? 'api/updateSocial' : 'api/saveSocial';
        this.http.post(url, this.data)
            .subscribe(data => {
            console.log(data);
        }, error => {
            console.log(error);
        });
        this.dialogRef.close();
    }
    cancel() {
        this.dialogRef.close();
    }
};
SocialDialogComponent = __decorate([
    Component({
        selector: 'app-social-dialog',
        templateUrl: './social-dialog.component.html',
        styleUrls: ['./social-dialog.component.css']
    }),
    __param(2, Inject(MAT_DIALOG_DATA))
], SocialDialogComponent);
export { SocialDialogComponent };
//# sourceMappingURL=social-dialog.component.js.map