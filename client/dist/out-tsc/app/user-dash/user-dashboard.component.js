import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SocialDialogComponent } from '../social-dialog/social-dialog.component';
/**
 * @title Table with pagination
 */
let UserDashboardComponent = class UserDashboardComponent {
    constructor(router, dialog, http, authService) {
        this.router = router;
        this.dialog = dialog;
        this.http = http;
        this.authService = authService;
        this.displayedColumns = ['link', 'description', 'edit', 'delete'];
        this.newSocial = {};
        this.socialData = {};
        this.dataSource = new MatTableDataSource(this.socialData);
    }
    ngOnInit() {
        this.getSocials();
    }
    getUserName() {
        return JSON.parse(this.authService.currentUser).name;
    }
    getSocials() {
        console.log(JSON.parse(this.authService.currentUser));
        this.http.post('api/getSocialsByUser', { userId: JSON.parse(this.authService.currentUser).id })
            .subscribe(data => {
            console.log(data);
            this.socialData = data;
            this.dataSource = new MatTableDataSource(this.socialData);
        }, error => {
            console.log(error);
        });
    }
    addSocial() {
        console.log('add');
        this.newSocial.userId = JSON.parse(this.authService.currentUser).id;
        const dialogRef = this.dialog.open(SocialDialogComponent, {
            width: '20%',
            data: this.newSocial,
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.newSocial = {};
            this.getSocials();
        });
    }
    editSocial(element) {
        console.log('edit');
        const dialogRef = this.dialog.open(SocialDialogComponent, {
            width: '20%',
            data: element,
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.newSocial = {};
            this.getSocials();
        });
    }
    deleteSocial(element) {
        console.log('delete');
        console.log(element);
        this.http.post('api/deleteSocial', element)
            .subscribe(data => {
            console.log(data);
            this.getSocials();
        }, error => {
            console.log(error);
        });
    }
};
UserDashboardComponent = __decorate([
    Component({
        selector: 'user-dashboard',
        styleUrls: ['user-dashboard.component.css'],
        templateUrl: 'user-dashboard.component.html',
    })
], UserDashboardComponent);
export { UserDashboardComponent };
//# sourceMappingURL=user-dashboard.component.js.map