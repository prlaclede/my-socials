import { TestBed } from '@angular/core/testing';
import { UserDashboardComponent } from './user-dashboard.component';
describe('UserDashboardComponent', () => {
    let component;
    let fixture;
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [UserDashboardComponent]
        })
            .compileComponents();
        fixture = TestBed.createComponent(UserDashboardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=user-dashboard.component.spec.js.map