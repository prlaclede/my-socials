import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { UserDashboardComponent } from './user-dash/user-dashboard.component';
import { UserViewerComponent } from './user-viewer/user-viewer.component';
import { AuthGuardService } from './service/auth-guard.service';
const routes = [
    { path: 'login', component: LoginComponent },
    { path: 'userDash', component: UserDashboardComponent, canActivate: [AuthGuardService] },
    { path: 'userView', component: UserViewerComponent },
    { path: '**', component: LoginComponent }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forRoot(routes)],
        exports: [RouterModule]
    })
], AppRoutingModule);
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map