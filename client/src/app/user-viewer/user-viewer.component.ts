import { AfterViewInit, Component, ViewChild, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SocialDialogComponent } from '../social-dialog/social-dialog.component';
import { ActivatedRoute } from '@angular/router';

/**
 * @title Table with pagination
 */
@Component({
  selector: 'user-viewer',
  templateUrl: './user-viewer.component.html',
  styleUrls: ['./user-viewer.component.css']
})
export class UserViewerComponent {
  displayedColumns: string[] = ['link', 'description'];
  urlParams: any;
  socialData: any = {};
  userInfo: any = {};
  dataSource = new MatTableDataSource<any>(this.socialData);

  constructor(private route: ActivatedRoute, public dialog: MatDialog, private http: HttpClient) {
  }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.urlParams = params;
        if (this.urlParams.userId) {
          this.getUserInfo();
          this.getSocials();
        }
      })
  }

  public getUserName() {
    if (this.userInfo.length > 0) {
      return this.userInfo[0].name
    }
  }

  public getUserInfo() {
    this.http.post('api/getUserInfo', { userId: this.urlParams.userId })
      .subscribe(
        data => {
          console.log(data);
          this.userInfo = data;
        },
        error => {
          console.log(error);
        }
      )
  }

  //114712794227286721354
  public getSocials() {
    this.http.post('api/getSocialsByUser', { userId: this.urlParams.userId })
      .subscribe(
        data => {
          console.log(data);
          this.socialData = data
          this.dataSource = new MatTableDataSource<any>(this.socialData);
        },
        error => {
          console.log(error);
        }
      )
  }

}
