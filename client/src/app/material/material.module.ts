import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';

import { LayoutModule } from '@angular/cdk/layout';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';



const materialModules = [
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatListModule,
    MatMenuModule,
    MatSelectModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatGridListModule,
    MatDatepickerModule,
    MatProgressBarModule,
    MatExpansionModule,
    MatSidenavModule,
    MatInputModule,
    MatSnackBarModule,
    MatTableModule,
    MatToolbarModule,
    /* CDK Modules */
    LayoutModule,
    FlexLayoutModule 
];

/**
 * 
 * 
 * @export
 * @class MaterialModule
 */
@NgModule({
    imports: [
        materialModules,
        BrowserAnimationsModule,
        FormsModule,
        MatTabsModule,
        MatDialogModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule
    ],
    exports: [
        materialModules,
        BrowserAnimationsModule,
        FormsModule,
        MatTabsModule,
        MatDialogModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule
    ],
    declarations: [],
    providers: [MatIconRegistry],
})
export class MaterialModule { }
