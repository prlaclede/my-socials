import { AfterViewInit, Component, ViewChild, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SocialDialogComponent } from '../social-dialog/social-dialog.component';
import { Router } from '@angular/router';

import { AuthService } from '../service/auth.service';


/**
 * @title Table with pagination
 */
@Component({
  selector: 'user-dashboard',
  styleUrls: ['user-dashboard.component.css'],
  templateUrl: 'user-dashboard.component.html',
})
export class UserDashboardComponent {
  displayedColumns: string[] = ['link', 'description', 'edit', 'delete'];
  newSocial: any = {};
  socialData: any = {};
  dataSource = new MatTableDataSource<any>(this.socialData);

  constructor(private router: Router, public dialog: MatDialog, private http: HttpClient, private authService: AuthService) {
  }

  ngOnInit() {
    this.getSocials()
  }

  public getUserName() {
    return JSON.parse(this.authService.currentUser).name
  }

  public getSocials() {
    console.log(JSON.parse(this.authService.currentUser))
    this.http.post('api/getSocialsByUser', { userId: JSON.parse(this.authService.currentUser).id })
      .subscribe(
        data => {
          console.log(data);
          this.socialData = data
          this.dataSource = new MatTableDataSource<any>(this.socialData);
        },
        error => {
          console.log(error);
        }
      )
  }

  public addSocial() {
    console.log('add')
    this.newSocial.userId = JSON.parse(this.authService.currentUser).id
    const dialogRef = this.dialog.open(SocialDialogComponent, {
      width: '20%',
      data: this.newSocial,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.newSocial = {};
      this.getSocials();
    });
  }

  public editSocial(element: any) {
    console.log('edit')
    const dialogRef = this.dialog.open(SocialDialogComponent, {
      width: '20%',
      data: element,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.newSocial = {};
      this.getSocials();
    });
  }

  public deleteSocial(element: any) {
    console.log('delete')
    console.log(element)
    this.http.post('api/deleteSocial', element)
      .subscribe(
        data => {
          console.log(data);
          this.getSocials();
        },
        error => {
          console.log(error);
        }
      )
  }
}