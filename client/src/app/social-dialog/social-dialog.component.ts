import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-social-dialog',
  templateUrl: './social-dialog.component.html',
  styleUrls: ['./social-dialog.component.css']
})
export class SocialDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<SocialDialogComponent>,
    private http: HttpClient,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  save(): void {
    var url = (this.data._id) ? 'api/updateSocial' : 'api/saveSocial'
    
    this.http.post(url, this.data)
      .subscribe(
        data => {
          console.log(data);
        },
        error => {
          console.log(error);
        }
      )
    this.dialogRef.close();
  }
    

  cancel(): void {
    this.dialogRef.close();
  }

}
