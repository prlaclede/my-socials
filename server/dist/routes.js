"use strict";
exports.__esModule = true;
var express = require("express");
var cors = require("cors");
var socials_mao_1 = require("./controllers/socials.mao");
var user_mao_1 = require("./controllers/user.mao");
function setRoutes(app, db) {
    var router = express.Router();
    //options for cors midddleware
    var options = {
        allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
        credentials: true,
        methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
        origin: "http://localhost:4200/",
        preflightContinue: false
    };
    router.use(cors(options));
    var userCtrl = new user_mao_1["default"]();
    var socialCtrl = new socials_mao_1["default"]();
    router.route('/testRouter').get(function () { console.log('router active!'); });
    router.route('/checkUser').post(userCtrl.checkUser);
    router.route('/getUserInfo').post(userCtrl.getUserInfo);
    router.route('/getSocialsByUser').post(socialCtrl.getSocialsByUser);
    router.route('/saveSocial').post(socialCtrl.saveSocial);
    router.route('/updateSocial').post(socialCtrl.updateSocial);
    router.route('/deleteSocial').post(socialCtrl.deleteSocial);
    router.options("*", cors(options));
    // Apply the routes to our application with the prefix /api
    app.use('/api', router);
}
exports["default"] = setRoutes;
