"use strict";
exports.__esModule = true;
exports.app = void 0;
var express = require("express");
var mongoose = require("mongoose");
var logger = require("morgan");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var q = require("q");
var routes_1 = require("./routes");
var app = express();
exports.app = app;
app.use(express.json());
app.listen(3000, function (err) {
    if (err) {
        return console.log(err);
    }
    return console.log('My Express App listening on port 3000');
});
//mount logger
app.use(logger("dev"));
app.use(cookieParser());
app.use(express.urlencoded({ extended: false }));
//mount json form parser
app.use(bodyParser.json());
//mount query string parser
app.use(bodyParser.urlencoded({
    extended: true
}));
global.Promise = q.Promise;
// mongoose.Promise = global.Promise;
var mongodbURI = 'mongodb+srv://MySocialsApp:2X22zxImDqua64nx@cluster0.b5qzh.mongodb.net/MySocialsApp?retryWrites=true&w=majority';
app.set('appDB', mongoose.connect(mongodbURI, { useNewUrlParser: true })
    .then(function (db) {
    console.log('Connected to MongoDB');
    (0, routes_1["default"])(app, db);
})["catch"](function (err) { return console.error(err); }));
