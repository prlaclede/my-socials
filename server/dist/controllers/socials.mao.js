"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var socials_1 = require("../models/socials");
var base_mao_1 = require("./base.mao");
var SocialsMao = /** @class */ (function (_super) {
    __extends(SocialsMao, _super);
    function SocialsMao() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.model = new socials_1["default"]().SocialsModel;
        _this.getSocialsByUser = function (req, res) {
            _this.getAll(req, res);
        };
        _this.saveSocial = function (req, res) {
            _this.create(req, res);
        };
        _this.updateSocial = function (req, res) {
            _this.update(req, res);
        };
        _this.deleteSocial = function (req, res) {
            _this["delete"](req, res);
        };
        return _this;
    }
    return SocialsMao;
}(base_mao_1["default"]));
exports["default"] = SocialsMao;
