"use strict";
exports.__esModule = true;
var BaseMao = /** @class */ (function () {
    function BaseMao() {
        var _this = this;
        // Get all
        this.getAll = function (req, res) {
            console.log(req.body);
            _this.model.find({ userId: req.body.userId }, function (err, docs) {
                if (err) {
                    return console.error(err);
                }
                return res.status(200).json(docs);
            });
        };
        // Count all
        this.count = function (req, res) {
            _this.model.count(function (err, count) {
                if (err) {
                    return console.error(err);
                }
                res.status(200).json(count);
            });
        };
        // Create
        this.create = function (req, res) {
            var obj = new _this.model(req.body);
            obj.save(function (err, item) {
                // 11000 is the code for duplicate key error
                if (err && err.code === 11000) {
                    res.sendStatus(400);
                }
                if (err) {
                    return console.error(err);
                }
                res.status(200).json(item);
            });
        };
        // Get by id
        this.get = function (req, res) {
            _this.model.findOne({ _id: req.body._id }, function (err, item) {
                if (err) {
                    return console.error(err);
                }
                res.status(200).json(item);
            });
        };
        // Update by id
        this.update = function (req, res) {
            _this.model.findOneAndUpdate({ _id: req.body._id }, req.body, function (err) {
                if (err) {
                    return console.error(err);
                }
                res.sendStatus(200);
            });
        };
        // Delete by id
        this["delete"] = function (req, res) {
            _this.model.deleteOne({ _id: req.body._id }, function (err) {
                if (err) {
                    return console.error(err);
                }
                return res.status(200).json({});
            });
        };
    }
    return BaseMao;
}());
exports["default"] = BaseMao;
