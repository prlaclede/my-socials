"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var users_1 = require("../models/users");
var base_mao_1 = require("./base.mao");
var jwt = require('jsonwebtoken');
var OAuth2Client = require('google-auth-library').OAuth2Client;
var appCLient = '35804208116-3hjqs624qaa8gnvlmop8vs09813mloit.apps.googleusercontent.com';
var client = new OAuth2Client(appCLient);
var UserMAO = /** @class */ (function (_super) {
    __extends(UserMAO, _super);
    function UserMAO() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.model = new users_1["default"]().UserModel;
        _this.getUserInfo = function (req, res) {
            _this.getAll(req, res);
        };
        _this.checkUser = function (req, res) {
            var g_csrf_token_req = req.body.g_csrf_token;
            var g_csrf_token_cookie = req.cookies.g_csrf_token;
            if (g_csrf_token_req === g_csrf_token_cookie) {
                var gAuth_1 = jwt.decode(req.body.credential);
                _this.model.find({ id: gAuth_1.sub }).exec().then(function (e) {
                    console.log(e);
                    if (e.length === 0) {
                        var obj = new _this.model({
                            userId: gAuth_1.sub,
                            name: gAuth_1.name,
                            email: gAuth_1.email
                        });
                        obj.save(function (err, item) {
                            if (err) {
                                return console.error(err);
                            }
                        });
                    }
                    res.cookie('user', JSON.stringify({
                        userId: gAuth_1.sub,
                        name: gAuth_1.name,
                        email: gAuth_1.email
                    }));
                    res.redirect('http://localhost:4200/userDash');
                });
            }
            return false;
        };
        return _this;
    }
    return UserMAO;
}(base_mao_1["default"]));
exports["default"] = UserMAO;
