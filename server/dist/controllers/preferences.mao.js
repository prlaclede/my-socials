"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var preferences_1 = require("../models/preferences");
var brandImages_chunks_1 = require("../models/brandImages.chunks");
var brandImages_files_1 = require("../models/brandImages.files");
var base_mao_1 = require("./base.mao");
var mongoose = require('mongoose');
var fs = require('fs');
var multer = require('multer');
var GridFsStorage = require("multer-gridfs-storage");
var mongo = require('mongodb');
var Grid = require('gridfs-stream');
var gfs = Grid(mongoose.db + "/brandImages", mongo);
var PreferencesMao = /** @class */ (function (_super) {
    __extends(PreferencesMao, _super);
    function PreferencesMao() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.preferences = new preferences_1["default"]();
        _this.brandImageChunks = new brandImages_chunks_1["default"]();
        _this.brandImageFiles = new brandImages_files_1["default"]();
        _this.model = _this.preferences.PreferencesModel;
        _this.brandImageChunksModel = _this.brandImageChunks.BrandImagesChunksModel;
        _this.brandImageFilesModel = _this.brandImageFiles.BrandImagesFilesModel;
        _this.storage = new GridFsStorage({
            // url: 'mongodb+srv://MySocialsApp:rs8wgnmzEkvKERg@cluster0.b5qzh.mongodb.net/MySocials?retryWrites=true&w=majority',
            gfs: gfs,
            options: { useNewUrlParser: true, useUnifiedTopology: true },
            file: function (req, file) {
                return {
                    bucketName: 'brandImages',
                    //Setting collection name, default name is fs      
                    filename: file.originalname
                    //Setting file name to original name of file    
                };
            }
        });
        _this.upload = multer({
            storage: _this.storage
        }).single("brandImage");
        _this.getUserPreference = function (req, res) {
            var queryItem = { userID: req.body._id };
            _this.model.find(queryItem, function (err, preference) {
                res.status(200).json(preference);
            });
        };
        _this.getBrandImage = function (req, res) {
            var queryItem = { userID: req.body._id };
            _this.model.find(queryItem, function (err, preference) {
                if (err) {
                    console.log(err);
                    res.status(500).json(err);
                }
                else {
                    var fileData = [];
                    var returnFile;
                    _this.brandImageChunksModel.find({ 'files_id': mongoose.Types.ObjectId(preference[0].brandImage) })
                        .sort({ n: 1 }).exec(function (err, fileChunks) {
                        if (err) {
                            console.log(err);
                        }
                        for (var i = 0; i < fileChunks.length; i++) {
                            //This is in Binary JSON or BSON format, which is stored               
                            //in fileData array in base64 endocoded string format               
                            fileData.push(fileChunks[i].data.toString('base64'));
                        }
                        _this.brandImageFilesModel.find({ '_id': mongoose.Types.ObjectId(preference[0].brandImage) }, function (err, fileObj) {
                            if (err) {
                                console.log(err);
                            }
                            else {
                                // set the proper content type 
                                var readstream = gfs.createReadStream({
                                    filename: fileObj[0].filename,
                                    root: "brandImages"
                                });
                                // set the proper content type 
                                res.set('Content-Type', fileObj[0].contentType);
                                // Return response
                                return readstream.pipe(res);
                                // returnFile = 'data:' + fileObj[0].contentType + ';base64,' + fileData.join('');
                                // // returnFile = fileData.join('');
                                // res.status(200).json(returnFile);
                            }
                        });
                    });
                }
            });
        };
        _this.savePreference = function (req, res) {
            _this.upload(req, res, function (err) {
                if (err) {
                    return res.status(200).json(err);
                }
                var preferenceParams = req.body;
                var newPreference = _this.preferences;
                newPreference.userId = preferenceParams.userId;
                newPreference.backgroundColor = preferenceParams.backgroundColor;
                newPreference.primaryColor = preferenceParams.primaryColor;
                newPreference.secondaryColor = preferenceParams.secondaryColor;
                newPreference.primaryTextColor = preferenceParams.primaryTextColor;
                newPreference.secondaryTextColor = preferenceParams.secondaryTextColor;
                newPreference.brandImage = req.file.id;
                var $this = _this;
                $this.deleteOldBrandImage(preferenceParams.userId).then(function () {
                    console.log('preference delete return');
                    $this.deletePreference(preferenceParams.userId).then(function () {
                        console.log('save preference');
                        newPreference.save(function (err, item) {
                            if (err) {
                                return res.status(200).json(err);
                            }
                            else if (item) {
                                console.log('saved preference');
                                return res.status(200).json(item);
                            }
                        });
                    });
                });
            });
        };
        _this.deleteOldBrandImage = function (userId) { return __awaiter(_this, void 0, void 0, function () {
            var queryItem;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        queryItem = { userId: userId };
                        return [4 /*yield*/, this.model.findOne(queryItem, function (err, item) {
                                if (err) {
                                    console.log(err);
                                }
                                else if (item) {
                                    _this.brandImageChunksModel.findOneAndDelete({ 'files_id': mongoose.Types.ObjectId(item.brandImage) }, function (err, item) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        else {
                                            console.log('removed old image chunks');
                                        }
                                    });
                                    _this.brandImageFilesModel.findOneAndDelete({ '_id': mongoose.Types.ObjectId(item.brandImage) }, function (err, item) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        else {
                                            console.log('removed old image file');
                                        }
                                    });
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); };
        _this.deletePreference = function (userId) { return __awaiter(_this, void 0, void 0, function () {
            var queryItem;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        queryItem = { userId: userId };
                        return [4 /*yield*/, this.model.findOneAndDelete(queryItem, function (err, item) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    console.log('removed old preference');
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); };
        return _this;
    }
    return PreferencesMao;
}(base_mao_1["default"]));
exports["default"] = PreferencesMao;
