"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var Socials = /** @class */ (function () {
    function Socials() {
        this.socialsSchema = new mongoose.Schema({
            userId: String,
            name: String,
            link: String,
            description: String
        });
        this.SocialsModel = mongoose.model('Socials', this.socialsSchema, 'Socials');
    }
    Socials.prototype.setUserId = function (userId) { this.userId = userId; };
    Socials.prototype.setName = function (name) { this.name = name; };
    Socials.prototype.setLink = function (link) { this.link = link; };
    Socials.prototype.setDescription = function (description) { this.description = description; };
    return Socials;
}());
exports["default"] = Socials;
