"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var Preferences = /** @class */ (function () {
    function Preferences() {
        this.preferencesSchema = new mongoose.Schema({
            userId: String,
            brandImage: String,
            backgroundColor: String,
            primaryColor: String,
            secondaryColor: String,
            primaryTextColor: String,
            secondaryTextColor: String
        });
        this.PreferencesModel = mongoose.model('Preferences', this.preferencesSchema, 'Preferences');
    }
    Preferences.prototype.setUserId = function (userId) { this.userId = userId; };
    Preferences.prototype.setBrandImage = function (brandImage) { this.brandImage = brandImage; };
    Preferences.prototype.setPrimaryColor = function (primaryColor) { this.primaryColor = primaryColor; };
    Preferences.prototype.setSecondaryColor = function (secondaryColor) { this.secondaryColor = secondaryColor; };
    Preferences.prototype.setPrimaryTextColor = function (primaryTextColor) { this.primaryTextColor = primaryTextColor; };
    Preferences.prototype.setSecondaryTextColor = function (secondaryTextColor) { this.secondaryTextColor = secondaryTextColor; };
    return Preferences;
}());
exports["default"] = Preferences;
