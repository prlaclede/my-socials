"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var Users = /** @class */ (function () {
    function Users() {
        this.userSchema = new mongoose.Schema({
            id: String,
            email: String,
            name: String
        });
        this.UserModel = mongoose.model('Users', this.userSchema, 'Users');
    }
    Users.prototype.setID = function (id) { this.id = id; };
    Users.prototype.setEmail = function (email) { this.email = email; };
    Users.prototype.setName = function (name) { this.name = name; };
    return Users;
}());
exports["default"] = Users;
