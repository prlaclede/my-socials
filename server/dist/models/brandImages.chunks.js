"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var BrandImagesChunks = /** @class */ (function () {
    function BrandImagesChunks() {
        this.BrandImagesChunksSchema = new mongoose.Schema({
            n: Number,
            data: String
        });
        this.BrandImagesChunksModel = mongoose.model('brandImages.chunks', this.BrandImagesChunksSchema, 'brandImages.chunks');
    }
    return BrandImagesChunks;
}());
exports["default"] = BrandImagesChunks;
