"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var BrandImagesFiles = /** @class */ (function () {
    function BrandImagesFiles() {
        this.BrandImagesFilesSchema = new mongoose.Schema({
            length: Number,
            chunkSize: Number,
            uploadDate: Date,
            fileName: String,
            mds: String,
            contentType: String
        });
        this.BrandImagesFilesModel = mongoose.model('brandImages.files', this.BrandImagesFilesSchema, 'brandImages.files');
    }
    return BrandImagesFiles;
}());
exports["default"] = BrandImagesFiles;
