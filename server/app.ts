import * as express from 'express';
import * as mongoose from 'mongoose';
import * as logger from "morgan";
import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser"
import * as q from "q";
import setRoutes from './routes';

const app = express();

app.use(express.json());

app.listen(3000, err => {
  if (err) {
    return console.log(err);
  }
  return console.log('My Express App listening on port 3000');
});

//mount logger
app.use(logger("dev"));

app.use(cookieParser());

app.use(express.urlencoded({ extended: false }));
//mount json form parser
app.use(bodyParser.json());

//mount query string parser
app.use(bodyParser.urlencoded({
  extended: true
}));

global.Promise = q.Promise;
// mongoose.Promise = global.Promise;

let mongodbURI = 'mongodb+srv://MySocialsApp:2X22zxImDqua64nx@cluster0.b5qzh.mongodb.net/MySocialsApp?retryWrites=true&w=majority';

app.set('appDB', mongoose.connect(mongodbURI, { useNewUrlParser: true })
  .then(db => {
    console.log('Connected to MongoDB');
    setRoutes(app, db);
  })
  .catch(err => console.error(err)));

export { app };