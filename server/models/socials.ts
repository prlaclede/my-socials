import * as mongoose from 'mongoose';

export default class Socials {
    constructor() { }

    userId: String;
    name: String;
    link: String;
    description: String;

    public setUserId(userId) { this.userId = userId; }

    public setName(name) { this.name = name; }

    public setLink(link) { this.link = link; }

    public setDescription(description) { this.description = description; }
    
    private socialsSchema = new mongoose.Schema({
        userId: String,
        name: String,
        link: String,
        description: String
    });

    public SocialsModel = mongoose.model('Socials', this.socialsSchema, 'Socials');

}