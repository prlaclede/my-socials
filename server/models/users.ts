import * as mongoose from 'mongoose';

export default class Users {
    constructor() { }

    id: String;
    email: String;
    name: String

    public setID(id) { this.id = id; }

    public setEmail(email) { this.email = email; }

    public setName(name) { this.name = name; }

    private userSchema = new mongoose.Schema({
        id: String,
        email: String,
        name: String,
    });

    public UserModel = mongoose.model('Users', this.userSchema, 'Users');
}