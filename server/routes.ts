import * as express from 'express';
import * as request from 'request';

import * as cors from "cors";

import SocialCtrl from './controllers/socials.mao';
import UserCtrl from './controllers/user.mao';

export default function setRoutes(app, db) {

  const router = express.Router();

  //options for cors midddleware
  const options: cors.CorsOptions = {
    allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
    credentials: true,
    methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin: "http://localhost:4200/",
    preflightContinue: false
  };

  router.use(cors(options));

  const userCtrl = new UserCtrl();
  const socialCtrl = new SocialCtrl();
  router.route('/testRouter').get(function () { console.log('router active!'); });

  router.route('/checkUser').post(userCtrl.checkUser);
  router.route('/getUserInfo').post(userCtrl.getUserInfo);

  router.route('/getSocialsByUser').post(socialCtrl.getSocialsByUser);
  
  router.route('/saveSocial').post(socialCtrl.saveSocial);
  router.route('/updateSocial').post(socialCtrl.updateSocial);
  router.route('/deleteSocial').post(socialCtrl.deleteSocial);

  router.options("*", cors(options));

  // Apply the routes to our application with the prefix /api
  app.use('/api', router);

}