abstract class BaseMao {

    abstract model: any;

    // Get all
    getAll = (req, res) => {
        console.log(req.body)
        this.model.find({ userId: req.body.userId }, (err, docs) => {
            if (err) { return console.error(err); }
            return res.status(200).json(docs);
        });
    }

    // Count all
    count = (req, res) => {
        this.model.count((err, count) => {
            if (err) { return console.error(err); }
            res.status(200).json(count);
        });
    }

    // Create
    create = (req, res) => {
        const obj = new this.model(req.body);
        obj.save((err, item) => {
            // 11000 is the code for duplicate key error
            if (err && err.code === 11000) {
                res.sendStatus(400);
            }
            if (err) {
                return console.error(err);
            }
            res.status(200).json(item);
        });
    }

    // Get by id
    get = (req, res) => {
        this.model.findOne({ _id: req.body._id }, (err, item) => {
            if (err) { return console.error(err); }
            res.status(200).json(item);
        });
    }

    // Update by id
    update = (req, res) => {
        this.model.findOneAndUpdate({ _id: req.body._id }, req.body, (err) => {
            if (err) { return console.error(err); }
            res.sendStatus(200);
        });
    }

    // Delete by id
    delete = (req, res) => {
        this.model.deleteOne({ _id: req.body._id }, (err) => {
            if (err) { return console.error(err); }
            return res.status(200).json({});
        });
    }
}

export default BaseMao;