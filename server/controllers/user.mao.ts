import Users from '../models/users';
import BaseMao from './base.mao';

var jwt = require('jsonwebtoken');
const { OAuth2Client } = require('google-auth-library');
const appCLient = '35804208116-3hjqs624qaa8gnvlmop8vs09813mloit.apps.googleusercontent.com'
const client = new OAuth2Client(appCLient);

export default class UserMAO extends BaseMao {
    model = new Users().UserModel;

    getUserInfo = (req, res) => {
        this.getAll(req, res)
    }

    checkUser = (req, res) => {
        const g_csrf_token_req = req.body.g_csrf_token
        const g_csrf_token_cookie = req.cookies.g_csrf_token

        if (g_csrf_token_req === g_csrf_token_cookie) {
            const gAuth = jwt.decode(req.body.credential)
            this.model.find({ id: gAuth.sub }).exec().then((e) => {
                console.log(e)
                if (e.length === 0) {
                    const obj = new this.model(
                        {
                            userId: gAuth.sub,
                            name: gAuth.name,
                            email: gAuth.email
                        }
                    );
                    obj.save((err, item) => {
                        if (err) {
                            return console.error(err);
                        }
                    });
                }
                res.cookie('user',
                    JSON.stringify({
                        userId: gAuth.sub,
                        name: gAuth.name,
                        email: gAuth.email
                    })
                )
                res.redirect('http://localhost:4200/userDash')
            })
        }
        return false
    }
}