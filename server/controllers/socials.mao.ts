import Socials from '../models/socials';
import BaseMao from './base.mao';

export default class SocialsMao extends BaseMao {
    model = new Socials().SocialsModel

    getSocialsByUser = (req, res) => {
        this.getAll(req, res)
    }

    saveSocial = (req, res) => {       
        this.create(req, res)
    }

    updateSocial = (req, res) => {
        this.update(req, res)
    }

    deleteSocial = (req, res) => {
        this.delete(req, res)
    }
}